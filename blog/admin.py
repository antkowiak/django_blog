# -*- coding: utf-8 -*-

from django.contrib import admin
from blog.models import Wpis, Komentarz, Kategoria


class ChoiceInline(admin.TabularInline):
    model = Komentarz
    extra = 3


class WpisAdmin(admin.ModelAdmin):
    fieldsets = [
        ('None', {'fields': ('autor','kategoria','tytul', 'tresc')})

        ]

    inlines = [ChoiceInline]
    list_display = ('tytul', 'kategoria', 'tresc', 'data_utworzenia')


class KategoriaAdmin(admin.ModelAdmin):
    list_display = ('id','nazwa')

admin.site.register(Wpis, WpisAdmin) 
admin.site.register(Kategoria, KategoriaAdmin) 