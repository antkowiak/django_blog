# -*- coding: utf-8 -*-

from django.db import models
from django.utils import timezone


class Kategoria(models.Model):
	id = models.AutoField(primary_key=True)
	nazwa = models.CharField(max_length=100)
	def __str__(self):
		return self.nazwa
	
	def __unicode__(self):
		return self.nazwa

class Wpis(models.Model):
	id = models.AutoField(primary_key=True)
	autor = models.ForeignKey('auth.User')
	tytul = models.CharField(max_length=100)
	tresc = models.TextField()
	kategoria = models.ForeignKey(Kategoria)
	data_utworzenia = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return self.tytul

	def __unicode__(self):
		return self.tytul


	def was_published_recently(self):
		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)


class Komentarz(models.Model):
	wpis_id = models.ForeignKey(Wpis)
	nick = models.CharField(max_length=20)
	tresc = models.TextField()
	pub_date = models.DateTimeField(default=timezone.now)
 
	def __str__(self):
		return self.tresc

	def __unicode__(self):
		return self.tresc