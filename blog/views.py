# -*- coding: utf-8 -*
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from .models import Wpis
from .models import Komentarz
from .forms import FormularzKomentarza


def lista_wpisow(request):

	wpisy = Wpis.objects.order_by('-data_utworzenia')
	return render(request, 'blog/lista_wpisow.html', {'wpisy': wpisy}) 


def pojedynczy_wpis(request, pk):
	wpis = get_object_or_404(Wpis, pk=pk)
	wpis_id = Wpis.objects.get(pk=pk)

	komentarze = list(Komentarz.objects.values('nick', 'tresc', 'pub_date').filter(wpis_id=wpis_id).order_by('-pub_date'))
	status = u""

	formularz = FormularzKomentarza(request.POST or None)
	if formularz.is_valid():
		zapisz = formularz.save(commit=False)
		zapisz.wpis_id = wpis_id
		zapisz.save()
		status = True
		formularz = FormularzKomentarza()
		komentarze = list(Komentarz.objects.values('nick', 'tresc', 'pub_date').filter(wpis_id=wpis_id).order_by('pub_date'))


	return render(request, 'blog/pojedynczy_wpis.html', {'formularz':formularz,'wpis': wpis, 'status': status, 'komentarze': komentarze})




def szukaj(request,stri):
	wpisy = Wpis.objects.filter(tytul__icontains=stri).order_by('-data_utworzenia')| Wpis.objects.filter(tresc__icontains=stri).order_by('-data_utworzenia')
	return render(request, 'blog/szukaj.html', {'wpisy': wpisy, 'stri': stri}) 



def kategoria(request,stri):
	wpisy = Wpis.objects.filter(kategoria__nazwa__icontains=stri).order_by('-data_utworzenia')| Wpis.objects.filter(tresc__icontains=stri).order_by('-data_utworzenia')
	return render(request, 'blog/kategoria.html', {'wpisy': wpisy, 'stri': stri}) 



