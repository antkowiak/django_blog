from django import forms
from .models import Wpis

from .models import Komentarz

class FormularzKomentarza(forms.ModelForm):

    class Meta:
        model = Komentarz
        fields = ('nick', 'tresc',)