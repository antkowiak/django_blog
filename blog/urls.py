from django.conf.urls import patterns, include, url
from . import views

urlpatterns = patterns('',
    url(r'^$', views.lista_wpisow),
    url(r'^wpis/(?P<pk>[0-9]+)/$', views.pojedynczy_wpis),
    url(r'^szukaj/(?P<stri>.+)/$', views.szukaj),
    url(r'^kategoria/(?P<stri>.+)/$', views.kategoria),

)